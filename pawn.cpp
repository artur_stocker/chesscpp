#include "pawn.h"

bool Pawn::checkMove(int x, int y, bool e)
{
	if (abs(y) > 2) return false;
	if (owner == WHITE && y >= 1 && e == false && x == 0) return true;
	if (owner == WHITE && y == 1 && e == true && abs(x) == 1) return true;
	if (owner == BLACK && y <= -1 && e == false && x == 0) return true;
	if (owner == BLACK && y == -1 && e == true && abs(x) == 1) return true;
	return false;
}
