#include "knight.h"

bool Knight::checkMove(int x, int y, bool e)
{
	if ((abs(x) == 2 || abs(y) == 2) && (abs(x) + abs(y)) == 3) return true;
	return false;
}