#ifndef ROOK_H
#define ROOK_H
#include "piece.h"

/// @brief	Rook class.
class Rook : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Rook(color c) : Piece(c) {};

	bool checkMove(int, int, bool) override;

};

#endif // ROOK_H
