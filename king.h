#ifndef KING_H
#define KING_H
#include "piece.h"

/// @brief	King class.
class King : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	King(color c) : Piece(c) {};

	bool checkMove(int, int, bool) override;
};

#endif // KING_H
