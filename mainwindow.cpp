#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <QStatusBar>

#include "chess.h"

Chess chess;
int count = 0;
position p1 = { 0,0 };
position p2 = { 0,0 };


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_Game_triggered()
{
	chess.startNewGame();
	clearTiles();
	drawTiles();
}

void MainWindow::on_actionSave_Game_triggered()
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Save game"), "", "Text files(*.txt)");
	chess.saveGame(filename.toLocal8Bit().constData());
}

void MainWindow::on_actionLoad_Game_triggered()
{
	QString filename = QFileDialog::getOpenFileName(this,tr("Open saved game"),"","Text files(*.txt)");
	QMessageBox::StandardButton load = QMessageBox::question(this, tr("Load save file"), ("Do you want load save file?"));
	if (load == QMessageBox::Yes) chess.loadGame(filename.toLocal8Bit().constData());
	clearTiles();
	drawTiles();
}

void MainWindow::on_actionExit_triggered()
{
	QMessageBox::StandardButton quit = QMessageBox::question(this, tr("Exit ChessCPP"), ("Do you want close program?"));
	if (quit == QMessageBox::Yes) QApplication::quit();
}

void MainWindow::on_selectAction(position pos) 
{
	if (count == 0 && chess.containPiece(pos)) {
		p1 = pos;
		count++;
	}
	else if (count == 1) {
		p2 = pos;
		count--;
		if (chess.doMove(p1, p2)) 
		{
			clearTiles();
			drawTiles();
		}
	}
}

void MainWindow::drawTiles()
{
	vector<pieceInfo> pieces = chess.returnBoard();

	for (auto& p : pieces) {
		emit setTile(p);
	}
	
	QString state = QString::fromStdString(chess.checkState());
	statusBar()->showMessage(state);
}

void MainWindow::clearTiles()
{
	pieceInfo clear = { 0,0,"clear","" };
	emit setTile(clear);
}
