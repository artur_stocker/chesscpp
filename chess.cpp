#include "chess.h"

Chess::Chess()
{
}

Chess::~Chess()
{
	board.~Board();
}

bool Chess::startNewGame()
{
	vector<pieceInfo> startPieces = {
		{1,1,"Rook","WHITE"}, {2,1,"Knight","WHITE"}, {3,1,"Bishop","WHITE"}, {4,1,"Queen","WHITE"}, {5,1,"King","WHITE"}, {6,1,"Bishop","WHITE"}, {7,1,"Knight","WHITE"}, {8,1,"Rook","WHITE"},
		{1,2,"Pawn","WHITE"}, {2,2,"Pawn","WHITE"}, {3,2,"Pawn","WHITE"}, {4,2,"Pawn","WHITE"}, {5,2,"Pawn","WHITE"}, {6,2,"Pawn","WHITE"}, {7,2,"Pawn","WHITE"}, {8,2,"Pawn","WHITE"},
		{1,8,"Rook","BLACK"}, {2,8,"Knight","BLACK"}, {3,8,"Bishop","BLACK"}, {4,8,"Queen","BLACK"}, {5,8,"King","BLACK"}, {6,8,"Bishop","BLACK"}, {7,8,"Knight","BLACK"}, {8,8,"Rook","BLACK"},
		{1,7,"Pawn","BLACK"}, {2,7,"Pawn","BLACK"}, {3,7,"Pawn","BLACK"}, {4,7,"Pawn","BLACK"}, {5,7,"Pawn","BLACK"}, {6,7,"Pawn","BLACK"}, {7,7,"Pawn","BLACK"}, {8,7,"Pawn","BLACK"}
	};

	if (!clearBoard()) return false;

	try {
		for (auto &p : startPieces) {
			board.setSquare(p);
		}
		turn = WHITE;
		state = "White turn";
		return true;
	}
	catch (...) {
		clearBoard();
		return false;
	}
}

vector<pieceInfo> Chess::returnBoard()
{
	piecies.clear();

	for (int x = 1; x <= BOARDSIZE; x++) {
		for (int y = 1; y <= BOARDSIZE; y++)
		{
			position pos = { x,y };
			if (board.getSquare(pos)->getPiece() != nullptr) {
				piecies.push_back(board.getSquare(pos)->getInfo());
			}
		}
	}

	return piecies;
}

bool Chess::doMove(position from, position to)
{
	// Check if piece can do that move.
	if (!checkMove(from, to)) return false;

	// Checking if that player turn.
	if (!(board.getSquare(from)->getPiece()->getColor() == turn)) return false;

	// Prepare pointers.
	Square* fromSquare = board.getSquare(from);
	Square* toSquare = board.getSquare(to);
	Piece* fromPiece = fromSquare->getPiece();
	Piece* toPiece = toSquare->getPiece();

	// Change position of piece.
	toSquare->setPiece(fromPiece);
	fromSquare->setPiece(nullptr);

	if (!doCheck(turn))
	{
		if(toPiece != nullptr) toPiece->~Piece();
	}
	else {
		state = "King in check!";

		fromSquare->setPiece(fromPiece);
		toSquare->setPiece(toPiece);
		return false;
	}

	// Change a turn.
	turn == WHITE ? turn = BLACK : turn = WHITE;
	if (turn == WHITE) {
		state = "White turn";
	} else { 
		state = "Black turn"; 
	}

	if (doCheck(turn))
	{
		state += " - King in check!";
	}

	return true;
}

string Chess::checkState()
{
	return state;
}

bool Chess::saveGame(string fileName)
{
	std::fstream file;
	file.open(fileName, std::ios::out, std::ios::trunc);
	if (file.is_open()) {
		file << turn << "\n";
		file << state << "\n";

		vector<pieceInfo> pieces = this->returnBoard();
		
		for (auto &p : pieces) {
			file << p.x << " " << p.y << " " << p.piece << " " << p.color << "\n";
		}
		file.flush();
		file.close();
	}
	else return false;
	return true;
}

bool Chess::loadGame(string fileName)
{
	vector<pieceInfo> piecesToLoad;

	std::fstream file;
	file.open(fileName, std::ios::in);
	if (file.is_open()) {
		try
		{
			int t;
			file >> t;
			if (t == 0) turn = WHITE;
			else turn = BLACK;

			file.ignore();
			std::getline(file, state);

			pieceInfo piece;
			while (!file.eof())
			{
				file >> piece.x;
				file >> piece.y;
				file >> piece.piece;
				file >> piece.color;
				piecesToLoad.push_back(piece);
			}

		}
		catch (...)
		{
			file.close();
			return false;
		}		
		file.close();
	}
	else return false;
	
	if (!clearBoard()) return false;

	for (auto &p : piecesToLoad) {
		board.setSquare(p);
	}

	return true;
}

vector<position> Chess::availableMoves(position pos)
{
	vector<position> moves;
	if (board.getSquare(pos)->getPiece() == nullptr) return moves;
	for (int i = 1; i <= BOARDSIZE; i++) {
		for (int j = 1; j <= BOARDSIZE; j++) {
			position to = { i,j };
			if (checkMove(pos, to)) moves.push_back(to);
		}
	}
	return moves;
}

bool Chess::containPiece(position pos)
{
	if (board.getSquare(pos)->getPiece() != nullptr) return true;
	return false;
}

bool Chess::checkMove(position from, position to) {
	// Checking if square in board and contain piece.
	if (from.x > BOARDSIZE || from.x < 1 || from.y > BOARDSIZE || from.y < 1) return false;
	if (to.x > BOARDSIZE || to.x < 1 || to.y > BOARDSIZE || to.y < 1) return false;
	if (board.getSquare(from)->getPiece() == nullptr) return false;

	// Preparing arguments
	int difX = to.x - from.x;
	int difY = to.y - from.y;
	bool targetContainEnemy = false;

	// Checking if target isn't the same player piece
	if (board.getSquare(to)->getPiece() != nullptr) {
		if (board.getSquare(from)->getPiece()->getColor() == board.getSquare(to)->getPiece()->getColor()) return false;
		else targetContainEnemy = true;
	}

	Square* startSquare = board.getSquare(from);
	Square* endSquare = board.getSquare(to);

	// Checking if piece can do that move.
	if (!startSquare->getPiece()->checkMove(difX, difY, targetContainEnemy)) return false;

	// Additional check for PAWN: 2 square moves depending of row
	if (typeid(*startSquare->getPiece()) == typeid(Pawn)) {
		if (abs(difY) == 2 && !(from.y == 2 || from.y == BOARDSIZE - 1)) return false;
	}

	//TODO: Check if clear path to target square
	if (difY == 0 && difX > 0) {
		for (int i = from.x + 1; i < to.x; i++) {
			position pos = { i, from.y };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
		}
	}
	if (difY == 0 && difX < 0) {
		for (int i = from.x - 1; i > to.x; i--) {
			position pos = { i, from.y };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
		}
	}
	if (difX == 0 && difY > 0) {
		for (int i = from.y + 1; i < to.y; i++) {
			position pos = { from.x, i };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
		}
	}
	if (difX == 0 && difY < 0) {
		for (int i = from.y - 1; i > to.y; i--) {
			position pos = { from.x, i };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
		}
	}
	if (abs(difX) == abs(difY) && difX > 0 && difY > 0) {
		int j = from.y + 1;
		for (int i = from.x + 1; i < to.x; i++) {
			position pos = { i, j };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
			j++;
		}
	}
	if (abs(difX) == abs(difY) && difX > 0 && difY < 0) {
		int j = from.y - 1;
		for (int i = from.x + 1; i < to.x; i++) {
			position pos = { i, j };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
			j--;
		}
	}
	if (abs(difX) == abs(difY) && difX < 0 && difY > 0) {
		int j = from.y + 1;
		for (int i = from.x - 1; i > to.x; i--) {
			position pos = { i, j };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
			j++;
		}
	}
	if (abs(difX) == abs(difY) && difX < 0 && difY < 0) {
		int j = from.y - 1;
		for (int i = from.x - 1; i > to.x; i--) {
			position pos = { i, j };
			if (board.getSquare(pos)->getPiece() != nullptr) return false;
			j--;
		}
	}

	return true;
}


bool Chess::doCheck(color col) {
	Square* sqKing = nullptr;
	position posKing = { 0,0 };
	for (int i = 1; i <= BOARDSIZE; i++) {
		for (int j = 1; j <= BOARDSIZE; j++) {
			position pos = { i,j };
			Piece* piece = board.getSquare(pos)->getPiece();
			if (piece != nullptr) {
				if (piece->getColor() == col && typeid(*piece).name() == typeid(King).name()) {
					sqKing = board.getSquare(pos);
					posKing = pos;
					break;
				}
			}
		}
		if (sqKing != nullptr) break;
	}
	if (sqKing == nullptr) {
		return true;  //king in check
	}

	for (int i = 1; i <= BOARDSIZE; i++) {
		for (int j = 1; j <= BOARDSIZE; j++) {
			position pos = { i,j };
			Piece* piece = board.getSquare(pos)->getPiece();
			if (piece != nullptr && piece->getColor() != col) {
				if (checkMove(pos, posKing)) {
					return true; //king in check
				}
			}
		}
	}

	return false; //not in check
}

bool Chess::clearBoard()
{
	try
	{
		for (int i = 1; i <= BOARDSIZE; i++) {
			for (int j = 1; j <= BOARDSIZE; j++) {
				position pos = { i,j };
				board.getSquare(pos)->setPiece(nullptr);
			}
		}
	}
	catch (...)
	{
		return false;
	}
	return true;
}