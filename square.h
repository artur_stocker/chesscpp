#ifndef SQUARE_H
#define SQUARE_H
#include <piece.h>
#include <pawn.h>
#include <knight.h>
#include <bishop.h>
#include <rook.h>
#include <queen.h>
#include <king.h>


/// @brief	struct position
struct position
{
	/// @brief	X coordinate.
	///
	/// Should be valued 1 - 8.
	int x;

	/// @brief	Y coordinate.
	///
	/// Should be valued 1 - 8.
	int y;
};


/// @brief	Square class.
///
///	Container for piece.
class Square
{
public:
	/// @brief Constuctor of empty square.
	///
	///	Default position = {0, 0}, and piece = nullptr.
	Square();

	/// @brief Constuctor of square with given position.
	///
	///	Default piece = nullptr.
	///
	///	@param pos	Position of the square.
	Square(position pos);
	~Square();

	/// @brief Return pointer to piece that is contained in square.
	///
	/// @return		Piece *
	///
	///	@see		Piece
	Piece* getPiece();

	/// @brief Put piece to square.
	///
	///	@param p	Pointer to piece that should be put to square.
	/// @return		True if operation was successful.
	///
	///	@see		Piece
	bool setPiece(Piece* p);

	/// @brief Return information about contained piece.
	///
	///	Before using check if square contain any piece.
	///
	/// @return		pieceInfo
	///
	/// @see		pieceInfo
	pieceInfo getInfo();

private:
	position pos;
	Piece* piece;
};

#endif // SQUARE_H
