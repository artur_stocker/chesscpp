#ifndef PAWN_H
#define PAWN_H
#include "piece.h"

/// @brief	Pawn class.
class Pawn : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Pawn(color c) : Piece(c) {};

	virtual bool checkMove(int, int, bool) override;

};

#endif // PAWN_H
