#ifndef BISHOP_H
#define BISHOP_H
#include "piece.h"

/// @brief	Bishop class.

class Bishop : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Bishop(color c) : Piece(c) {};

	bool checkMove(int, int, bool) override;
};

#endif // BISHOP_H
