#ifndef QUEEN_H
#define QUEEN_H
#include "piece.h"

/// @brief	Queen class.
class Queen : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Queen(color c) : Piece(c) {};

	bool checkMove(int, int, bool) override;

};

#endif // QUEEN_H
