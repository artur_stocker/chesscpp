#include "tile.h"
#include <string>
#include <qsize.h>

Tile::Tile(QWidget *parent) : QPushButton(parent)
{

}

void Tile::drawTile(pieceInfo pI)
{
	string name = "";
	name = this->objectName().toLocal8Bit().constData();

	pos.x = (int)name.at(0) - 'a' + 1;
	pos.y = name.at(1) - '0';

	if (pos.x == pI.x && pos.y == pI.y) {
		string piece = pI.piece;
		string color = pI.color;

		if (piece == "Pawn" && color == "WHITE") this->setIcon(QIcon(":/resources/images/pawn_white.svg"));
		else if (piece == "Knight" && color == "WHITE") this->setIcon(QIcon(":/resources/images/knight_white.svg"));
		else if (piece == "Bishop" && color == "WHITE") this->setIcon(QIcon(":/resources/images/bishop_white.svg"));
		else if (piece == "Rook" && color == "WHITE") this->setIcon(QIcon(":/resources/images/rook_white.svg"));
		else if (piece == "Queen" && color == "WHITE") this->setIcon(QIcon(":/resources/images/queen_white.svg"));
		else if (piece == "King" && color == "WHITE") this->setIcon(QIcon(":/resources/images/king_white.svg"));
		else if (piece == "Pawn" && color == "BLACK") this->setIcon(QIcon(":/resources/images/pawn_black.svg"));
		else if (piece == "Knight" && color == "BLACK") this->setIcon(QIcon(":/resources/images/knight_black.svg"));
		else if (piece == "Bishop" && color == "BLACK") this->setIcon(QIcon(":/resources/images/bishop_black.svg"));
		else if (piece == "Rook" && color == "BLACK") this->setIcon(QIcon(":/resources/images/rook_black.svg"));
		else if (piece == "Queen" && color == "BLACK") this->setIcon(QIcon(":/resources/images/queen_black.svg"));
		else if (piece == "King" && color == "BLACK") this->setIcon(QIcon(":/resources/images/king_black.svg"));

		QSize size(64, 64);
		this->setIconSize(size);

		return;
	}

	if (pI.piece == "clear") {
		this->setIcon(QIcon(":/resources/images/empty.svg"));
	}
}

void Tile::mousePressEvent(QMouseEvent* e)
{
	emit select(pos);
	QPushButton::mousePressEvent(e);
}

