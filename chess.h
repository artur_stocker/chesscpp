#ifndef CHESS_H
#define CHESS_H
#include <board.h>

/// @brief	Main class that controls chess game.

class Chess
{
public:
	Chess();
	~Chess();

	/// @brief	Start new game.
	///
	///	Function clean and create new board of standard chess game.
	///
	/// @return		True if operation went succesful.
	bool startNewGame();

	/// @brief Return information about pieces on the board.
	///
	/// Function return vector of pieceInfo of existing pieces on the board.
	///
	/// @return		vector<pieceInfo>
	///
	///	@see		pieceInfo
	vector<pieceInfo> returnBoard();

	/// @brief Make a move.
	///
	///	Function check if move can be made (correct player turn, move for piece, king in check).
	///
	/// @param from	Position on board which piece should do a move.
	/// @param to	Position on board where moved piece should end.
	///	@return		True if move was correct and made.
	///
	///	@see		position
	bool doMove(position from, position to);

	/// @brief Chect state of a game.
	///	
	///	@return		String that contain text about current state of game.
	string checkState();

	/// @brief Save game to file.
	///
	///	File format for output is .txt.
	///
	///	@param fileName	String that contain path to desired file.
	///	@return			True if operation was successful.
	bool saveGame(string fileName);

	/// @brief Load game from file.
	///
	///	File format for input is .txt.
	///
	///	@param fileName	String that contain path to desired file.
	///	@return			True if operation was successful.
	bool loadGame(string fileName);

	/// @brief Return information of available moves for piece in that position on board.
	///
	///	@param pos	Position on board which pieces moves you want to check.
	/// @return		vector<position>
	///
	///	@see		position
	vector<position> availableMoves(position pos);

	/// @brief Return information if board contain piece in that position.
	///
	///	@param pos	Position on board which you want to check.
	/// @return		True if that position on board contain piece.
	///
	///	@see		position
	bool containPiece(position pos);

private:
	Board board;

	color turn;

	string state;

	vector<pieceInfo> piecies;

	bool checkMove(position, position);
	bool doCheck(color);

	bool clearBoard();
};



#endif // CHESS_H
