#ifndef PIECE_H
#define PIECE_H

#include <string>
using std::string;
#include <vector>
using std::vector;
#include <typeinfo>
#include <cmath>
#include <fstream>

/// @brief Standard board size.
const int BOARDSIZE = 8;

/// @brief Enum of piece colors.
enum color { WHITE, BLACK };

/// @brief struct pieceInfo
///
///	Container with information about piece.
struct pieceInfo
{
	/// @brief	X coordinate.
	int x;
	/// @brief	Y coordinate.
	int y;
	/// @brief	Piece name.
	string piece;
	/// @brief	Piece color.
	string color;
};

/// @brief	Piece class.

class Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Piece(color c);

	virtual ~Piece();

	/// @brief Check move for piece.
	///
	///	@param x	Movement in horizontal axis.
	///	@param y	Movement in vertical axis.
	///	@param e	Contain enemy.
	/// @return		True if move is posible.
	virtual bool checkMove(int x, int y, bool e);

	/// @brief Return color of piece.
	///
	/// @return		color
	///
	///	@see		color
	color getColor();

protected:
	color owner;
};

#endif // PIECE_H
