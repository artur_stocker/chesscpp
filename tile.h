#ifndef TILE_H
#define TILE_H

#include <QtWidgets/qpushbutton.h>
#include <piece.h>
#include <square.h>
#include <QObject>

/// @brief GUI class.

class Tile : public QPushButton
{
    Q_OBJECT

public:
    explicit Tile(QWidget* parent = nullptr);

	void Tile::mousePressEvent(QMouseEvent* e);

signals:
    void select(position);

private slots:
    void drawTile(pieceInfo);


private:
	position pos = { 1,1 };
};

#endif // TILE_H
