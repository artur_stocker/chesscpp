#include "square.h"

Square::Square()
{
	position def = { 0,0 };
	pos = def;
	piece = nullptr;
}

Square::Square(position pos)
{
	this->pos = pos;
	piece = nullptr;
}

Square::~Square()
{
	if (piece != nullptr) piece->~Piece();
}

Piece* Square::getPiece()
{
	return piece;
}

bool Square::setPiece(Piece* p)
{
	try {
		piece = p;
	}
	catch (...) {
		return false;
	}

	return true;
}

pieceInfo Square::getInfo()
{
	string piece;

	if (typeid(*getPiece()) == typeid(Pawn))
	{
		piece = "Pawn";
	}
	else if (typeid(*getPiece()) == typeid(Knight))
	{
		piece = "Knight";
	}
	else if (typeid(*getPiece()) == typeid(Bishop))
	{
		piece = "Bishop";
	}
	else if (typeid(*getPiece()) == typeid(Rook))
	{
		piece = "Rook";
	}
	else if (typeid(*getPiece()) == typeid(Queen))
	{
		piece = "Queen";
	}
	else if (typeid(*getPiece()) == typeid(King))
	{
		piece = "King";
	}

	string col;
	if (getPiece()->getColor() == WHITE) col = "WHITE";
	if (getPiece()->getColor() == BLACK) col = "BLACK";

	pieceInfo pI = { pos.x, pos.y, piece, col };

	return pI;
}
