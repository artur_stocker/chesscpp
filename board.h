#ifndef BOARD_H
#define BOARD_H
#include <square.h>

/// @brief	Board class.
///
///	Container for all squares of game.

class Board
{
public:
	/// @brief Create empty board.
	Board();
	~Board();

	/// @brief Return pointer to square in that position on board.
	///
	///	@param pos	Position on board.
	/// @return		Square *
	///
	///	@see		position
	///	@see		Square
	Square* getSquare(position pos);

	/// @brief Put piece on board.
	///
	///	@param pInfo	Position on board.
	/// @return			True if operation was successful.
	///
	///	@see			pieceInfo
	bool setSquare(pieceInfo pInfo);

private:
	Square square[8][8];
};

#endif // BOARD_H
