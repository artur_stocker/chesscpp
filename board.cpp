#include "board.h"

Board::Board()
{
	for (int x = 0; x < BOARDSIZE; x++) {
		for (int y = 0; y < BOARDSIZE; y++)
		{
			position pos = { x + 1,y + 1 };
			square[x][y].~Square();
			square[x][y] = Square(pos);
		}
	}
}

Board::~Board()
{
	for (auto& x : square)
	{
		for (auto& y : x) {
			y.~Square();
		}
	}
}

Square* Board::getSquare(position pos)
{
	return &square[pos.x - 1][pos.y - 1];
}

bool Board::setSquare(pieceInfo pInfo)
{
	position pos = { pInfo.x, pInfo.y };
	Square* tarSquare = getSquare(pos);

	color pieceColor;
	if (pInfo.color == "WHITE") pieceColor = WHITE;
	if (pInfo.color == "BLACK") pieceColor = BLACK;

	if (pInfo.piece == "Pawn")
	{
		tarSquare->setPiece(new Pawn(pieceColor));
	}
	else if (pInfo.piece == "Knight")
	{
		tarSquare->setPiece(new Knight(pieceColor));
	}
	else if (pInfo.piece == "Bishop")
	{
		tarSquare->setPiece(new Bishop(pieceColor));
	}
	else if (pInfo.piece == "Rook")
	{
		tarSquare->setPiece(new Rook(pieceColor));
	}
	else if (pInfo.piece == "Queen")
	{
		tarSquare->setPiece(new Queen(pieceColor));
	}
	else if (pInfo.piece == "King")
	{
		tarSquare->setPiece(new King(pieceColor));
	}
	else {
		return false;
	}

	return true;
}
