#ifndef KNIGHT_H
#define KNIGHT_H
#include "piece.h"

/// @brief	Knight class.
class Knight : public Piece
{
public:
	/// @brief Constuctor.
	///
	///	@param c	Color of piece.
	///
	/// @see		color
	Knight(color c) : Piece(c) {};

	bool checkMove(int, int, bool) override;

};

#endif // KNIGHT_H
