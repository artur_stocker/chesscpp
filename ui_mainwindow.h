/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "tile.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_Game;
    QAction *actionSave_Game;
    QAction *actionLoad_Game;
    QAction *actionExit;
    QWidget *gameBoard;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    Tile *f7;
    QLabel *label_6;
    Tile *a8;
    Tile *f2;
    Tile *g3;
    Tile *a4;
    QLabel *label_2;
    Tile *a2;
    Tile *c7;
    QLabel *label_7;
    Tile *a5;
    Tile *e1;
    Tile *h3;
    Tile *d7;
    Tile *c4;
    Tile *c1;
    Tile *f4;
    QLabel *label_4;
    Tile *h5;
    Tile *h4;
    Tile *b1;
    Tile *e8;
    Tile *d1;
    QLabel *label_5;
    Tile *g1;
    Tile *b7;
    Tile *d3;
    Tile *e6;
    Tile *e5;
    QLabel *label;
    Tile *g7;
    Tile *a7;
    Tile *h2;
    Tile *c6;
    Tile *g6;
    Tile *d4;
    Tile *f5;
    Tile *a1;
    Tile *e2;
    Tile *a6;
    Tile *h8;
    Tile *g5;
    Tile *b4;
    Tile *f8;
    Tile *b6;
    Tile *b2;
    Tile *d6;
    Tile *g8;
    Tile *h6;
    Tile *f3;
    Tile *e4;
    Tile *f1;
    Tile *c5;
    Tile *c8;
    Tile *g2;
    Tile *d5;
    QLabel *label_8;
    Tile *e7;
    Tile *a3;
    Tile *b8;
    Tile *h7;
    Tile *b3;
    Tile *h1;
    Tile *d2;
    Tile *e3;
    Tile *d8;
    Tile *f6;
    Tile *g4;
    Tile *c3;
    QLabel *label_3;
    Tile *b5;
    Tile *c2;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(550, 590);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(550, 590));
        MainWindow->setMaximumSize(QSize(550, 590));
        actionNew_Game = new QAction(MainWindow);
        actionNew_Game->setObjectName(QString::fromUtf8("actionNew_Game"));
        actionSave_Game = new QAction(MainWindow);
        actionSave_Game->setObjectName(QString::fromUtf8("actionSave_Game"));
        actionLoad_Game = new QAction(MainWindow);
        actionLoad_Game->setObjectName(QString::fromUtf8("actionLoad_Game"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        gameBoard = new QWidget(MainWindow);
        gameBoard->setObjectName(QString::fromUtf8("gameBoard"));
        sizePolicy.setHeightForWidth(gameBoard->sizePolicy().hasHeightForWidth());
        gameBoard->setSizePolicy(sizePolicy);
        gameBoard->setMinimumSize(QSize(550, 549));
        gameBoard->setMaximumSize(QSize(550, 549));
        gridLayoutWidget = new QWidget(gameBoard);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 546, 546));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(2);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout->setContentsMargins(1, 1, 0, 0);
        f7 = new Tile(gridLayoutWidget);
        f7->setObjectName(QString::fromUtf8("f7"));
        sizePolicy.setHeightForWidth(f7->sizePolicy().hasHeightForWidth());
        f7->setSizePolicy(sizePolicy);
        f7->setMinimumSize(QSize(64, 64));
        f7->setMaximumSize(QSize(64, 64));
        f7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f7, 1, 6, 1, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(16, 16));
        label_6->setMaximumSize(QSize(16, 16));
        label_6->setFrameShape(QFrame::NoFrame);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_6, 5, 0, 1, 1);

        a8 = new Tile(gridLayoutWidget);
        a8->setObjectName(QString::fromUtf8("a8"));
        sizePolicy.setHeightForWidth(a8->sizePolicy().hasHeightForWidth());
        a8->setSizePolicy(sizePolicy);
        a8->setMinimumSize(QSize(64, 64));
        a8->setMaximumSize(QSize(64, 64));
        a8->setBaseSize(QSize(16, 16));
        a8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a8, 0, 1, 1, 1);

        f2 = new Tile(gridLayoutWidget);
        f2->setObjectName(QString::fromUtf8("f2"));
        sizePolicy.setHeightForWidth(f2->sizePolicy().hasHeightForWidth());
        f2->setSizePolicy(sizePolicy);
        f2->setMinimumSize(QSize(64, 64));
        f2->setMaximumSize(QSize(64, 64));
        f2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f2, 6, 6, 1, 1);

        g3 = new Tile(gridLayoutWidget);
        g3->setObjectName(QString::fromUtf8("g3"));
        sizePolicy.setHeightForWidth(g3->sizePolicy().hasHeightForWidth());
        g3->setSizePolicy(sizePolicy);
        g3->setMinimumSize(QSize(64, 64));
        g3->setMaximumSize(QSize(64, 64));
        g3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g3, 5, 7, 1, 1);

        a4 = new Tile(gridLayoutWidget);
        a4->setObjectName(QString::fromUtf8("a4"));
        sizePolicy.setHeightForWidth(a4->sizePolicy().hasHeightForWidth());
        a4->setSizePolicy(sizePolicy);
        a4->setMinimumSize(QSize(64, 64));
        a4->setMaximumSize(QSize(64, 64));
        a4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a4, 4, 1, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(16, 16));
        label_2->setMaximumSize(QSize(16, 16));
        label_2->setFrameShape(QFrame::NoFrame);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        a2 = new Tile(gridLayoutWidget);
        a2->setObjectName(QString::fromUtf8("a2"));
        sizePolicy.setHeightForWidth(a2->sizePolicy().hasHeightForWidth());
        a2->setSizePolicy(sizePolicy);
        a2->setMinimumSize(QSize(64, 64));
        a2->setMaximumSize(QSize(64, 64));
        a2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a2, 6, 1, 1, 1);

        c7 = new Tile(gridLayoutWidget);
        c7->setObjectName(QString::fromUtf8("c7"));
        sizePolicy.setHeightForWidth(c7->sizePolicy().hasHeightForWidth());
        c7->setSizePolicy(sizePolicy);
        c7->setMinimumSize(QSize(64, 64));
        c7->setMaximumSize(QSize(64, 64));
        c7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c7, 1, 3, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(16, 16));
        label_7->setMaximumSize(QSize(16, 16));
        label_7->setFrameShape(QFrame::NoFrame);
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 6, 0, 1, 1);

        a5 = new Tile(gridLayoutWidget);
        a5->setObjectName(QString::fromUtf8("a5"));
        sizePolicy.setHeightForWidth(a5->sizePolicy().hasHeightForWidth());
        a5->setSizePolicy(sizePolicy);
        a5->setMinimumSize(QSize(64, 64));
        a5->setMaximumSize(QSize(64, 64));
        a5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a5, 3, 1, 1, 1, Qt::AlignVCenter);

        e1 = new Tile(gridLayoutWidget);
        e1->setObjectName(QString::fromUtf8("e1"));
        sizePolicy.setHeightForWidth(e1->sizePolicy().hasHeightForWidth());
        e1->setSizePolicy(sizePolicy);
        e1->setMinimumSize(QSize(64, 64));
        e1->setMaximumSize(QSize(64, 64));
        e1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e1, 7, 5, 1, 1);

        h3 = new Tile(gridLayoutWidget);
        h3->setObjectName(QString::fromUtf8("h3"));
        sizePolicy.setHeightForWidth(h3->sizePolicy().hasHeightForWidth());
        h3->setSizePolicy(sizePolicy);
        h3->setMinimumSize(QSize(64, 64));
        h3->setMaximumSize(QSize(64, 64));
        h3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h3, 5, 8, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        d7 = new Tile(gridLayoutWidget);
        d7->setObjectName(QString::fromUtf8("d7"));
        sizePolicy.setHeightForWidth(d7->sizePolicy().hasHeightForWidth());
        d7->setSizePolicy(sizePolicy);
        d7->setMinimumSize(QSize(64, 64));
        d7->setMaximumSize(QSize(64, 64));
        d7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d7, 1, 4, 1, 1);

        c4 = new Tile(gridLayoutWidget);
        c4->setObjectName(QString::fromUtf8("c4"));
        sizePolicy.setHeightForWidth(c4->sizePolicy().hasHeightForWidth());
        c4->setSizePolicy(sizePolicy);
        c4->setMinimumSize(QSize(64, 64));
        c4->setMaximumSize(QSize(64, 64));
        c4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c4, 4, 3, 1, 1);

        c1 = new Tile(gridLayoutWidget);
        c1->setObjectName(QString::fromUtf8("c1"));
        sizePolicy.setHeightForWidth(c1->sizePolicy().hasHeightForWidth());
        c1->setSizePolicy(sizePolicy);
        c1->setMinimumSize(QSize(64, 64));
        c1->setMaximumSize(QSize(64, 64));
        c1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c1, 7, 3, 1, 1);

        f4 = new Tile(gridLayoutWidget);
        f4->setObjectName(QString::fromUtf8("f4"));
        sizePolicy.setHeightForWidth(f4->sizePolicy().hasHeightForWidth());
        f4->setSizePolicy(sizePolicy);
        f4->setMinimumSize(QSize(64, 64));
        f4->setMaximumSize(QSize(64, 64));
        f4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f4, 4, 6, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(16, 16));
        label_4->setMaximumSize(QSize(16, 16));
        label_4->setFrameShape(QFrame::NoFrame);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_4, 3, 0, 1, 1, Qt::AlignHCenter);

        h5 = new Tile(gridLayoutWidget);
        h5->setObjectName(QString::fromUtf8("h5"));
        sizePolicy.setHeightForWidth(h5->sizePolicy().hasHeightForWidth());
        h5->setSizePolicy(sizePolicy);
        h5->setMinimumSize(QSize(64, 64));
        h5->setMaximumSize(QSize(64, 64));
        h5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h5, 3, 8, 1, 1);

        h4 = new Tile(gridLayoutWidget);
        h4->setObjectName(QString::fromUtf8("h4"));
        sizePolicy.setHeightForWidth(h4->sizePolicy().hasHeightForWidth());
        h4->setSizePolicy(sizePolicy);
        h4->setMinimumSize(QSize(64, 64));
        h4->setMaximumSize(QSize(64, 64));
        h4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h4, 4, 8, 1, 1, Qt::AlignVCenter);

        b1 = new Tile(gridLayoutWidget);
        b1->setObjectName(QString::fromUtf8("b1"));
        sizePolicy.setHeightForWidth(b1->sizePolicy().hasHeightForWidth());
        b1->setSizePolicy(sizePolicy);
        b1->setMinimumSize(QSize(64, 64));
        b1->setMaximumSize(QSize(64, 64));
        b1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b1, 7, 2, 1, 1);

        e8 = new Tile(gridLayoutWidget);
        e8->setObjectName(QString::fromUtf8("e8"));
        sizePolicy.setHeightForWidth(e8->sizePolicy().hasHeightForWidth());
        e8->setSizePolicy(sizePolicy);
        e8->setMinimumSize(QSize(64, 64));
        e8->setMaximumSize(QSize(64, 64));
        e8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e8, 0, 5, 1, 1);

        d1 = new Tile(gridLayoutWidget);
        d1->setObjectName(QString::fromUtf8("d1"));
        sizePolicy.setHeightForWidth(d1->sizePolicy().hasHeightForWidth());
        d1->setSizePolicy(sizePolicy);
        d1->setMinimumSize(QSize(64, 64));
        d1->setMaximumSize(QSize(64, 64));
        d1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d1, 7, 4, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(16, 16));
        label_5->setMaximumSize(QSize(16, 16));
        label_5->setFrameShape(QFrame::NoFrame);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        g1 = new Tile(gridLayoutWidget);
        g1->setObjectName(QString::fromUtf8("g1"));
        sizePolicy.setHeightForWidth(g1->sizePolicy().hasHeightForWidth());
        g1->setSizePolicy(sizePolicy);
        g1->setMinimumSize(QSize(64, 64));
        g1->setMaximumSize(QSize(64, 64));
        g1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g1, 7, 7, 1, 1);

        b7 = new Tile(gridLayoutWidget);
        b7->setObjectName(QString::fromUtf8("b7"));
        sizePolicy.setHeightForWidth(b7->sizePolicy().hasHeightForWidth());
        b7->setSizePolicy(sizePolicy);
        b7->setMinimumSize(QSize(64, 64));
        b7->setMaximumSize(QSize(64, 64));
        b7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b7, 1, 2, 1, 1);

        d3 = new Tile(gridLayoutWidget);
        d3->setObjectName(QString::fromUtf8("d3"));
        sizePolicy.setHeightForWidth(d3->sizePolicy().hasHeightForWidth());
        d3->setSizePolicy(sizePolicy);
        d3->setMinimumSize(QSize(64, 64));
        d3->setMaximumSize(QSize(64, 64));
        d3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d3, 5, 4, 1, 1);

        e6 = new Tile(gridLayoutWidget);
        e6->setObjectName(QString::fromUtf8("e6"));
        sizePolicy.setHeightForWidth(e6->sizePolicy().hasHeightForWidth());
        e6->setSizePolicy(sizePolicy);
        e6->setMinimumSize(QSize(64, 64));
        e6->setMaximumSize(QSize(64, 64));
        e6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e6, 2, 5, 1, 1);

        e5 = new Tile(gridLayoutWidget);
        e5->setObjectName(QString::fromUtf8("e5"));
        sizePolicy.setHeightForWidth(e5->sizePolicy().hasHeightForWidth());
        e5->setSizePolicy(sizePolicy);
        e5->setMinimumSize(QSize(64, 64));
        e5->setMaximumSize(QSize(64, 64));
        e5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e5, 3, 5, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(16, 16));
        label->setMaximumSize(QSize(16, 16));
        label->setFrameShape(QFrame::NoFrame);
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        g7 = new Tile(gridLayoutWidget);
        g7->setObjectName(QString::fromUtf8("g7"));
        sizePolicy.setHeightForWidth(g7->sizePolicy().hasHeightForWidth());
        g7->setSizePolicy(sizePolicy);
        g7->setMinimumSize(QSize(64, 64));
        g7->setMaximumSize(QSize(64, 64));
        g7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g7, 1, 7, 1, 1);

        a7 = new Tile(gridLayoutWidget);
        a7->setObjectName(QString::fromUtf8("a7"));
        sizePolicy.setHeightForWidth(a7->sizePolicy().hasHeightForWidth());
        a7->setSizePolicy(sizePolicy);
        a7->setMinimumSize(QSize(64, 64));
        a7->setMaximumSize(QSize(64, 64));
        a7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a7, 1, 1, 1, 1);

        h2 = new Tile(gridLayoutWidget);
        h2->setObjectName(QString::fromUtf8("h2"));
        sizePolicy.setHeightForWidth(h2->sizePolicy().hasHeightForWidth());
        h2->setSizePolicy(sizePolicy);
        h2->setMinimumSize(QSize(64, 64));
        h2->setMaximumSize(QSize(64, 64));
        h2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h2, 6, 8, 1, 1);

        c6 = new Tile(gridLayoutWidget);
        c6->setObjectName(QString::fromUtf8("c6"));
        sizePolicy.setHeightForWidth(c6->sizePolicy().hasHeightForWidth());
        c6->setSizePolicy(sizePolicy);
        c6->setMinimumSize(QSize(64, 64));
        c6->setMaximumSize(QSize(64, 64));
        c6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c6, 2, 3, 1, 1);

        g6 = new Tile(gridLayoutWidget);
        g6->setObjectName(QString::fromUtf8("g6"));
        sizePolicy.setHeightForWidth(g6->sizePolicy().hasHeightForWidth());
        g6->setSizePolicy(sizePolicy);
        g6->setMinimumSize(QSize(64, 64));
        g6->setMaximumSize(QSize(64, 64));
        g6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g6, 2, 7, 1, 1);

        d4 = new Tile(gridLayoutWidget);
        d4->setObjectName(QString::fromUtf8("d4"));
        sizePolicy.setHeightForWidth(d4->sizePolicy().hasHeightForWidth());
        d4->setSizePolicy(sizePolicy);
        d4->setMinimumSize(QSize(64, 64));
        d4->setMaximumSize(QSize(64, 64));
        d4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d4, 4, 4, 1, 1);

        f5 = new Tile(gridLayoutWidget);
        f5->setObjectName(QString::fromUtf8("f5"));
        sizePolicy.setHeightForWidth(f5->sizePolicy().hasHeightForWidth());
        f5->setSizePolicy(sizePolicy);
        f5->setMinimumSize(QSize(64, 64));
        f5->setMaximumSize(QSize(64, 64));
        f5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f5, 3, 6, 1, 1);

        a1 = new Tile(gridLayoutWidget);
        a1->setObjectName(QString::fromUtf8("a1"));
        sizePolicy.setHeightForWidth(a1->sizePolicy().hasHeightForWidth());
        a1->setSizePolicy(sizePolicy);
        a1->setMinimumSize(QSize(64, 64));
        a1->setMaximumSize(QSize(64, 64));
        a1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a1, 7, 1, 1, 1);

        e2 = new Tile(gridLayoutWidget);
        e2->setObjectName(QString::fromUtf8("e2"));
        sizePolicy.setHeightForWidth(e2->sizePolicy().hasHeightForWidth());
        e2->setSizePolicy(sizePolicy);
        e2->setMinimumSize(QSize(64, 64));
        e2->setMaximumSize(QSize(64, 64));
        e2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e2, 6, 5, 1, 1);

        a6 = new Tile(gridLayoutWidget);
        a6->setObjectName(QString::fromUtf8("a6"));
        sizePolicy.setHeightForWidth(a6->sizePolicy().hasHeightForWidth());
        a6->setSizePolicy(sizePolicy);
        a6->setMinimumSize(QSize(64, 64));
        a6->setMaximumSize(QSize(64, 64));
        a6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a6, 2, 1, 1, 1);

        h8 = new Tile(gridLayoutWidget);
        h8->setObjectName(QString::fromUtf8("h8"));
        sizePolicy.setHeightForWidth(h8->sizePolicy().hasHeightForWidth());
        h8->setSizePolicy(sizePolicy);
        h8->setMinimumSize(QSize(64, 64));
        h8->setMaximumSize(QSize(64, 64));
        h8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h8, 0, 8, 1, 1, Qt::AlignHCenter);

        g5 = new Tile(gridLayoutWidget);
        g5->setObjectName(QString::fromUtf8("g5"));
        sizePolicy.setHeightForWidth(g5->sizePolicy().hasHeightForWidth());
        g5->setSizePolicy(sizePolicy);
        g5->setMinimumSize(QSize(64, 64));
        g5->setMaximumSize(QSize(64, 64));
        g5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g5, 3, 7, 1, 1);

        b4 = new Tile(gridLayoutWidget);
        b4->setObjectName(QString::fromUtf8("b4"));
        sizePolicy.setHeightForWidth(b4->sizePolicy().hasHeightForWidth());
        b4->setSizePolicy(sizePolicy);
        b4->setMinimumSize(QSize(64, 64));
        b4->setMaximumSize(QSize(64, 64));
        b4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b4, 4, 2, 1, 1);

        f8 = new Tile(gridLayoutWidget);
        f8->setObjectName(QString::fromUtf8("f8"));
        sizePolicy.setHeightForWidth(f8->sizePolicy().hasHeightForWidth());
        f8->setSizePolicy(sizePolicy);
        f8->setMinimumSize(QSize(64, 64));
        f8->setMaximumSize(QSize(64, 64));
        f8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f8, 0, 6, 1, 1);

        b6 = new Tile(gridLayoutWidget);
        b6->setObjectName(QString::fromUtf8("b6"));
        sizePolicy.setHeightForWidth(b6->sizePolicy().hasHeightForWidth());
        b6->setSizePolicy(sizePolicy);
        b6->setMinimumSize(QSize(64, 64));
        b6->setMaximumSize(QSize(64, 64));
        b6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b6, 2, 2, 1, 1);

        b2 = new Tile(gridLayoutWidget);
        b2->setObjectName(QString::fromUtf8("b2"));
        sizePolicy.setHeightForWidth(b2->sizePolicy().hasHeightForWidth());
        b2->setSizePolicy(sizePolicy);
        b2->setMinimumSize(QSize(64, 64));
        b2->setMaximumSize(QSize(64, 64));
        b2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b2, 6, 2, 1, 1);

        d6 = new Tile(gridLayoutWidget);
        d6->setObjectName(QString::fromUtf8("d6"));
        sizePolicy.setHeightForWidth(d6->sizePolicy().hasHeightForWidth());
        d6->setSizePolicy(sizePolicy);
        d6->setMinimumSize(QSize(64, 64));
        d6->setMaximumSize(QSize(64, 64));
        d6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d6, 2, 4, 1, 1);

        g8 = new Tile(gridLayoutWidget);
        g8->setObjectName(QString::fromUtf8("g8"));
        sizePolicy.setHeightForWidth(g8->sizePolicy().hasHeightForWidth());
        g8->setSizePolicy(sizePolicy);
        g8->setMinimumSize(QSize(64, 64));
        g8->setMaximumSize(QSize(64, 64));
        g8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g8, 0, 7, 1, 1);

        h6 = new Tile(gridLayoutWidget);
        h6->setObjectName(QString::fromUtf8("h6"));
        sizePolicy.setHeightForWidth(h6->sizePolicy().hasHeightForWidth());
        h6->setSizePolicy(sizePolicy);
        h6->setMinimumSize(QSize(64, 64));
        h6->setMaximumSize(QSize(64, 64));
        h6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h6, 2, 8, 1, 1, Qt::AlignHCenter);

        f3 = new Tile(gridLayoutWidget);
        f3->setObjectName(QString::fromUtf8("f3"));
        sizePolicy.setHeightForWidth(f3->sizePolicy().hasHeightForWidth());
        f3->setSizePolicy(sizePolicy);
        f3->setMinimumSize(QSize(64, 64));
        f3->setMaximumSize(QSize(64, 64));
        f3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f3, 5, 6, 1, 1);

        e4 = new Tile(gridLayoutWidget);
        e4->setObjectName(QString::fromUtf8("e4"));
        sizePolicy.setHeightForWidth(e4->sizePolicy().hasHeightForWidth());
        e4->setSizePolicy(sizePolicy);
        e4->setMinimumSize(QSize(64, 64));
        e4->setMaximumSize(QSize(64, 64));
        e4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e4, 4, 5, 1, 1);

        f1 = new Tile(gridLayoutWidget);
        f1->setObjectName(QString::fromUtf8("f1"));
        sizePolicy.setHeightForWidth(f1->sizePolicy().hasHeightForWidth());
        f1->setSizePolicy(sizePolicy);
        f1->setMinimumSize(QSize(64, 64));
        f1->setMaximumSize(QSize(64, 64));
        f1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f1, 7, 6, 1, 1);

        c5 = new Tile(gridLayoutWidget);
        c5->setObjectName(QString::fromUtf8("c5"));
        sizePolicy.setHeightForWidth(c5->sizePolicy().hasHeightForWidth());
        c5->setSizePolicy(sizePolicy);
        c5->setMinimumSize(QSize(64, 64));
        c5->setMaximumSize(QSize(64, 64));
        c5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c5, 3, 3, 1, 1);

        c8 = new Tile(gridLayoutWidget);
        c8->setObjectName(QString::fromUtf8("c8"));
        sizePolicy.setHeightForWidth(c8->sizePolicy().hasHeightForWidth());
        c8->setSizePolicy(sizePolicy);
        c8->setMinimumSize(QSize(64, 64));
        c8->setMaximumSize(QSize(64, 64));
        c8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c8, 0, 3, 1, 1);

        g2 = new Tile(gridLayoutWidget);
        g2->setObjectName(QString::fromUtf8("g2"));
        sizePolicy.setHeightForWidth(g2->sizePolicy().hasHeightForWidth());
        g2->setSizePolicy(sizePolicy);
        g2->setMinimumSize(QSize(64, 64));
        g2->setMaximumSize(QSize(64, 64));
        g2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g2, 6, 7, 1, 1);

        d5 = new Tile(gridLayoutWidget);
        d5->setObjectName(QString::fromUtf8("d5"));
        sizePolicy.setHeightForWidth(d5->sizePolicy().hasHeightForWidth());
        d5->setSizePolicy(sizePolicy);
        d5->setMinimumSize(QSize(64, 64));
        d5->setMaximumSize(QSize(64, 64));
        d5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d5, 3, 4, 1, 1);

        label_8 = new QLabel(gridLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(16, 16));
        label_8->setMaximumSize(QSize(16, 16));
        label_8->setFrameShape(QFrame::NoFrame);
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_8, 7, 0, 1, 1);

        e7 = new Tile(gridLayoutWidget);
        e7->setObjectName(QString::fromUtf8("e7"));
        sizePolicy.setHeightForWidth(e7->sizePolicy().hasHeightForWidth());
        e7->setSizePolicy(sizePolicy);
        e7->setMinimumSize(QSize(64, 64));
        e7->setMaximumSize(QSize(64, 64));
        e7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e7, 1, 5, 1, 1);

        a3 = new Tile(gridLayoutWidget);
        a3->setObjectName(QString::fromUtf8("a3"));
        sizePolicy.setHeightForWidth(a3->sizePolicy().hasHeightForWidth());
        a3->setSizePolicy(sizePolicy);
        a3->setMinimumSize(QSize(64, 64));
        a3->setMaximumSize(QSize(64, 64));
        a3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(a3, 5, 1, 1, 1);

        b8 = new Tile(gridLayoutWidget);
        b8->setObjectName(QString::fromUtf8("b8"));
        sizePolicy.setHeightForWidth(b8->sizePolicy().hasHeightForWidth());
        b8->setSizePolicy(sizePolicy);
        b8->setMinimumSize(QSize(64, 64));
        b8->setMaximumSize(QSize(64, 64));
        b8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b8, 0, 2, 1, 1);

        h7 = new Tile(gridLayoutWidget);
        h7->setObjectName(QString::fromUtf8("h7"));
        sizePolicy.setHeightForWidth(h7->sizePolicy().hasHeightForWidth());
        h7->setSizePolicy(sizePolicy);
        h7->setMinimumSize(QSize(64, 64));
        h7->setMaximumSize(QSize(64, 64));
        h7->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h7, 1, 8, 1, 1);

        b3 = new Tile(gridLayoutWidget);
        b3->setObjectName(QString::fromUtf8("b3"));
        sizePolicy.setHeightForWidth(b3->sizePolicy().hasHeightForWidth());
        b3->setSizePolicy(sizePolicy);
        b3->setMinimumSize(QSize(64, 64));
        b3->setMaximumSize(QSize(64, 64));
        b3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b3, 5, 2, 1, 1);

        h1 = new Tile(gridLayoutWidget);
        h1->setObjectName(QString::fromUtf8("h1"));
        sizePolicy.setHeightForWidth(h1->sizePolicy().hasHeightForWidth());
        h1->setSizePolicy(sizePolicy);
        h1->setMinimumSize(QSize(64, 64));
        h1->setMaximumSize(QSize(64, 64));
        h1->setIconSize(QSize(40, 40));

        gridLayout->addWidget(h1, 7, 8, 1, 1);

        d2 = new Tile(gridLayoutWidget);
        d2->setObjectName(QString::fromUtf8("d2"));
        sizePolicy.setHeightForWidth(d2->sizePolicy().hasHeightForWidth());
        d2->setSizePolicy(sizePolicy);
        d2->setMinimumSize(QSize(64, 64));
        d2->setMaximumSize(QSize(64, 64));
        d2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d2, 6, 4, 1, 1);

        e3 = new Tile(gridLayoutWidget);
        e3->setObjectName(QString::fromUtf8("e3"));
        sizePolicy.setHeightForWidth(e3->sizePolicy().hasHeightForWidth());
        e3->setSizePolicy(sizePolicy);
        e3->setMinimumSize(QSize(64, 64));
        e3->setMaximumSize(QSize(64, 64));
        e3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(e3, 5, 5, 1, 1);

        d8 = new Tile(gridLayoutWidget);
        d8->setObjectName(QString::fromUtf8("d8"));
        sizePolicy.setHeightForWidth(d8->sizePolicy().hasHeightForWidth());
        d8->setSizePolicy(sizePolicy);
        d8->setMinimumSize(QSize(64, 64));
        d8->setMaximumSize(QSize(64, 64));
        d8->setIconSize(QSize(40, 40));

        gridLayout->addWidget(d8, 0, 4, 1, 1);

        f6 = new Tile(gridLayoutWidget);
        f6->setObjectName(QString::fromUtf8("f6"));
        sizePolicy.setHeightForWidth(f6->sizePolicy().hasHeightForWidth());
        f6->setSizePolicy(sizePolicy);
        f6->setMinimumSize(QSize(64, 64));
        f6->setMaximumSize(QSize(64, 64));
        f6->setIconSize(QSize(40, 40));

        gridLayout->addWidget(f6, 2, 6, 1, 1);

        g4 = new Tile(gridLayoutWidget);
        g4->setObjectName(QString::fromUtf8("g4"));
        sizePolicy.setHeightForWidth(g4->sizePolicy().hasHeightForWidth());
        g4->setSizePolicy(sizePolicy);
        g4->setMinimumSize(QSize(64, 64));
        g4->setMaximumSize(QSize(64, 64));
        g4->setIconSize(QSize(40, 40));

        gridLayout->addWidget(g4, 4, 7, 1, 1);

        c3 = new Tile(gridLayoutWidget);
        c3->setObjectName(QString::fromUtf8("c3"));
        sizePolicy.setHeightForWidth(c3->sizePolicy().hasHeightForWidth());
        c3->setSizePolicy(sizePolicy);
        c3->setMinimumSize(QSize(64, 64));
        c3->setMaximumSize(QSize(64, 64));
        c3->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c3, 5, 3, 1, 1);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(16, 16));
        label_3->setMaximumSize(QSize(16, 16));
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        b5 = new Tile(gridLayoutWidget);
        b5->setObjectName(QString::fromUtf8("b5"));
        sizePolicy.setHeightForWidth(b5->sizePolicy().hasHeightForWidth());
        b5->setSizePolicy(sizePolicy);
        b5->setMinimumSize(QSize(64, 64));
        b5->setMaximumSize(QSize(64, 64));
        b5->setIconSize(QSize(40, 40));

        gridLayout->addWidget(b5, 3, 2, 1, 1);

        c2 = new Tile(gridLayoutWidget);
        c2->setObjectName(QString::fromUtf8("c2"));
        sizePolicy.setHeightForWidth(c2->sizePolicy().hasHeightForWidth());
        c2->setSizePolicy(sizePolicy);
        c2->setMinimumSize(QSize(64, 64));
        c2->setMaximumSize(QSize(64, 64));
        c2->setIconSize(QSize(40, 40));

        gridLayout->addWidget(c2, 6, 3, 1, 1);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(16, 16));
        label_9->setMaximumSize(QSize(16, 16));
        label_9->setFrameShape(QFrame::NoFrame);
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_9, 8, 1, 1, 1, Qt::AlignHCenter);

        label_10 = new QLabel(gridLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(16, 16));
        label_10->setMaximumSize(QSize(16, 16));
        label_10->setFrameShape(QFrame::NoFrame);
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_10, 8, 2, 1, 1, Qt::AlignHCenter);

        label_11 = new QLabel(gridLayoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(16, 16));
        label_11->setMaximumSize(QSize(16, 16));
        label_11->setFrameShape(QFrame::NoFrame);
        label_11->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_11, 8, 3, 1, 1, Qt::AlignHCenter);

        label_12 = new QLabel(gridLayoutWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(16, 16));
        label_12->setMaximumSize(QSize(16, 16));
        label_12->setFrameShape(QFrame::NoFrame);
        label_12->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_12, 8, 4, 1, 1, Qt::AlignHCenter);

        label_13 = new QLabel(gridLayoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(16, 16));
        label_13->setMaximumSize(QSize(16, 16));
        label_13->setFrameShape(QFrame::NoFrame);
        label_13->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_13, 8, 5, 1, 1, Qt::AlignHCenter);

        label_14 = new QLabel(gridLayoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(16, 16));
        label_14->setMaximumSize(QSize(16, 16));
        label_14->setFrameShape(QFrame::NoFrame);
        label_14->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_14, 8, 6, 1, 1, Qt::AlignHCenter);

        label_15 = new QLabel(gridLayoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setMinimumSize(QSize(16, 16));
        label_15->setMaximumSize(QSize(16, 16));
        label_15->setFrameShape(QFrame::NoFrame);
        label_15->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_15, 8, 7, 1, 1, Qt::AlignHCenter);

        label_16 = new QLabel(gridLayoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setMinimumSize(QSize(16, 16));
        label_16->setMaximumSize(QSize(16, 16));
        label_16->setFrameShape(QFrame::NoFrame);
        label_16->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_16, 8, 8, 1, 1, Qt::AlignHCenter);

        MainWindow->setCentralWidget(gameBoard);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 550, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionNew_Game);
        menuMenu->addSeparator();
        menuMenu->addAction(actionSave_Game);
        menuMenu->addAction(actionLoad_Game);
        menuMenu->addSeparator();
        menuMenu->addAction(actionExit);

        retranslateUi(MainWindow);
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h1, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h2, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a3, SLOT(drawTile(pieceInfo)));
        QObject::connect(a2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h3, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h4, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h5, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h6, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h7, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), a8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), b8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), c8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), d8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), e8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), f8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), g8, SLOT(drawTile(pieceInfo)));
        QObject::connect(MainWindow, SIGNAL(setTile(pieceInfo)), h8, SLOT(drawTile(pieceInfo)));
        QObject::connect(a8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f8, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h7, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h6, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h5, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(a4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h4, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h3, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h2, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(b1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(c1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(d1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(e1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(f1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(g1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));
        QObject::connect(h1, SIGNAL(select(position)), MainWindow, SLOT(on_selectAction(position)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "ChessCPP", nullptr));
        actionNew_Game->setText(QCoreApplication::translate("MainWindow", "New Game", nullptr));
        actionSave_Game->setText(QCoreApplication::translate("MainWindow", "Save Game", nullptr));
        actionLoad_Game->setText(QCoreApplication::translate("MainWindow", "Load Game", nullptr));
        actionExit->setText(QCoreApplication::translate("MainWindow", "Exit", nullptr));
        f7->setText(QString());
        label_6->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        a8->setText(QString());
        f2->setText(QString());
        g3->setText(QString());
        a4->setText(QString());
        label_2->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        a2->setText(QString());
        c7->setText(QString());
        label_7->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        a5->setText(QString());
        e1->setText(QString());
        h3->setText(QString());
        d7->setText(QString());
        c4->setText(QString());
        c1->setText(QString());
        f4->setText(QString());
        label_4->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        h5->setText(QString());
        h4->setText(QString());
        b1->setText(QString());
        e8->setText(QString());
        d1->setText(QString());
        label_5->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        g1->setText(QString());
        b7->setText(QString());
        d3->setText(QString());
        e6->setText(QString());
        e5->setText(QString());
        label->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        g7->setText(QString());
        a7->setText(QString());
        h2->setText(QString());
        c6->setText(QString());
        g6->setText(QString());
        d4->setText(QString());
        f5->setText(QString());
        a1->setText(QString());
        e2->setText(QString());
        a6->setText(QString());
        h8->setText(QString());
        g5->setText(QString());
        b4->setText(QString());
        f8->setText(QString());
        b6->setText(QString());
        b2->setText(QString());
        d6->setText(QString());
        g8->setText(QString());
        h6->setText(QString());
        f3->setText(QString());
        e4->setText(QString());
        f1->setText(QString());
        c5->setText(QString());
        c8->setText(QString());
        g2->setText(QString());
        d5->setText(QString());
        label_8->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        e7->setText(QString());
        a3->setText(QString());
        b8->setText(QString());
        h7->setText(QString());
        b3->setText(QString());
        h1->setText(QString());
        d2->setText(QString());
        e3->setText(QString());
        d8->setText(QString());
        f6->setText(QString());
        g4->setText(QString());
        c3->setText(QString());
        label_3->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        b5->setText(QString());
        c2->setText(QString());
        label_9->setText(QCoreApplication::translate("MainWindow", "A", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "B", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "C", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "D", nullptr));
        label_13->setText(QCoreApplication::translate("MainWindow", "E", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "F", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "G", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "H", nullptr));
        menuMenu->setTitle(QCoreApplication::translate("MainWindow", "Menu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
