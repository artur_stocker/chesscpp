#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "piece.h"
#include "square.h"


namespace Ui {
class MainWindow;
}
/// @brief GUI class.
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
	void setTile(pieceInfo);

private slots:
    void on_actionNew_Game_triggered();

    void on_actionSave_Game_triggered();

    void on_actionLoad_Game_triggered();

    void on_actionExit_triggered();

	void on_selectAction(position);

private:
    Ui::MainWindow *ui;
	void drawTiles();
	void clearTiles();

};

#endif // MAINWINDOW_H
