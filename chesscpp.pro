#-------------------------------------------------
#
# Project created by QtCreator 2019-09-02T20:00:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chesscpp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        bishop.cpp \
        board.cpp \
        chess.cpp \
        debug/moc_mainwindow.cpp \
        king.cpp \
        knight.cpp \
        main.cpp \
        mainwindow.cpp \
        pawn.cpp \
        piece.cpp \
        queen.cpp \
        rook.cpp \
        square.cpp \
        tile.cpp

HEADERS += \
        bishop.h \
        board.h \
        chess.h \
        debug/moc_predefs.h \
        king.h \
        knight.h \
        mainwindow.h \
        pawn.h \
        piece.h \
        queen.h \
        rook.h \
        square.h \
        tile.h \
        ui_mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES += \
    graphic/bishop_black.svg \
    graphic/bishop_white.svg \
    graphic/king_black.svg \
    graphic/king_white.svg \
    graphic/knight_black.svg \
    graphic/knight_white.svg \
    graphic/pawn_black.svg \
    graphic/pawn_white.svg \
    graphic/queen_black.svg \
    graphic/queen_white.svg \
    graphic/rook_black.svg \
    graphic/rook_white.svg
